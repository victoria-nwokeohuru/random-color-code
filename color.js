const primaryColors = ['red', 'yellow', 'blue'];
const secondaryColors = ['green', 'orange', 'violet', 'indigo', 'black', 'brown'];

const number = [...document.querySelectorAll('.buttonNumber')];
console.log(number)
const display = document.querySelector('#colorDisplay')

number.forEach((buttonNumber) => {
    buttonNumber.addEventListener('click', (event) => {
        const value = event.target.textContent;
        console.log(event.target.textContent);

        if (value % 2 !== 0) {
            colorDisplay.style.backgroundColor = secondaryColors[Math.floor(Math.random() * 7)];
        } 
        else {
            colorDisplay.style.backgroundColor = primaryColors[Math.floor(Math.random() * 3)];
        }
    })
})
